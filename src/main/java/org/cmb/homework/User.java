package org.cmb.homework;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class User {

    /**
     * 用户ID
     */
    private Integer userID;
    /**
     * 剩余可用的购买积分
     * Q: 有更好的数据结构吗？
     */
    private int remainingPoints;
    /**
     * 用户购买的商品记录
     * Q：选择一个更好的数据结构？
     */
    private int totalNum;
}
