package org.cmb.homework;

import org.cmb.homework.dao.GoodDao;
import org.cmb.homework.dao.UserDao;
import org.cmb.homework.entity.BuyLog;
import org.cmb.homework.service.OrderService;
import org.cmb.homework.util.Utils;

import java.util.List;


public class Main {

    /**
     * 类似于Springboot提供的属性注入，可以使用单例模式优化成单例对象（但是没有必要）
     */
    private static OrderService orderService = new OrderService();
    private static GoodDao goodDao = new GoodDao();
    private static UserDao userDao = new UserDao();


    public static void main(String[] args) {
        //1.获取商品信息
        List<Good> goodList = goodDao.getGoodList();

        //2.获取用户的信息
        List<User> userList = userDao.getUserList();

        //3.并发模拟多个用户的请购行为
        for (int i = 0; i < Global.USERS_NUM; i++) {
            int temp = i;
            new Thread(() -> {
                //每个用户默认的购买次数
                for (int j = 0; j < Global.DEFAULT_BUY_NUMS; j++) {
                    //获取随机的一个商品
                    Good good = goodList.get(Utils.generateRandomInt(goodList.size()));
                    //获取随机的购买数量
                    int nums = Utils.generateRandomInt(Global.BUY_NUMS);
                    //执行购买操作
                    if ( orderService.buyFD(userList.get(temp),good, nums) ){
                        System.out.println("购买成功");
                    }else {
                        System.out.println("购买失败");
                    }
                }
            }, Global.THREAD_PREFIX + userList.get(i)).start();
        }

        //这里是为了等所有的线程执行完成
        while (Thread.activeCount() > 2){

        }

        System.out.println("=====================================进行数据校验================================================");

        int totalGoodNum = 0;
        int totalOrderNum = 0;
        int checkGoodNum = 0;
        int totalUserMoney = 0;
        int checkUserMoney = 0;
        List<BuyLog> buyLogs = orderService.getBuyLogs();

        for (User user : userList) {
            totalOrderNum += user.getTotalNum();
            totalUserMoney += (Global.USER_DEFAULT_POINTS - user.getRemainingPoints());
            System.out.println("【系统提示】：最后的用户信息为：" + user);
        }

        for (Good good : goodList) {
            int consumeStock = Global.GOODS_RESOURCE_NUM - good.getInStock().getReference();
            totalGoodNum += consumeStock;
            checkUserMoney += (consumeStock * good.getPrice());
            System.out.println("【系统提示】：最后的商品信息为：" + good);
        }

        for (BuyLog buyLog : buyLogs) {
            checkGoodNum += buyLog.getBuyNum();
        }

        System.out.println("【系统提示】：最后的记录数量为：" + buyLogs.size());
        System.out.println("【系统提示】：最后的购买数量为：" + totalOrderNum);
        System.out.println("【系统提示】：消耗的库存数量为：" + totalGoodNum);
        System.out.println("【系统提示】：记录校验库存数量为：" + checkGoodNum);
        System.out.println("【系统提示】：用户消费的总金额为：" + totalUserMoney);
        System.out.println("【系统提示】：校验用户消费总金额为：" + checkUserMoney);
        System.out.println("nice !!!!");
    }
}
