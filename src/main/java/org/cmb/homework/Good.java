package org.cmb.homework;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.concurrent.atomic.AtomicStampedReference;

@Data
@Accessors(chain = true)
public class Good {

    /**
     * id
     */
    private int id;
    /**
     * 描述
     */
    private String description;
    /**
     * 带时间戳的原子引用类型----库存
     */
    private AtomicStampedReference<Integer> inStock = new AtomicStampedReference<>(Global.GOODS_RESOURCE_NUM, 0);
    /**
     * 价格
     */
    private int price;

    @Override
    public String toString() {
        return "Good{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", inStock=" + inStock.getReference() +
                ", price=" + price +
                '}';
    }
}
