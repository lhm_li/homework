package org.cmb.homework.dao;

import org.cmb.homework.Global;
import org.cmb.homework.User;
import org.cmb.homework.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class UserDao {

    private static List<User> userList = new ArrayList<>();

    static {
        for (int i = 0; i < Global.USERS_NUM; i++) {
            userList.add(Utils.generateUserEntity());
        }
    }

    /**
     * 查询所有的用户信息
     * @return List<User>
     */
    public List<User> getUserList(){
        return userList;
    }
}
