package org.cmb.homework.dao;

import org.cmb.homework.Global;
import org.cmb.homework.Good;
import org.cmb.homework.util.Utils;

import java.util.ArrayList;
import java.util.List;


public class GoodDao {

    private static List<Good> goodList = new ArrayList<>();

    static {
        for (int i = 0; i < Global.GOODS_NUM; i++) {
            goodList.add(Utils.generateGoodEntity());
        }
    }

    /**
     * 查询所有的商品信息
     * @return List<Good>
     */
    public List<Good> getGoodList(){
        return goodList;
    }


}
