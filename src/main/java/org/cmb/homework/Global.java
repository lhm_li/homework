package org.cmb.homework;

/**
 * Project Name: Java02
 * Package Name: org.cmb.homework.common
 * Class Name: org.cmb.homework.Global
 * Version:
 * Description:
 * Date: 2021-7-5 10:42
 *
 * @Author: yanggao yanggao@cmbchina.com
 * Copyright (c) ,China Merchants Bank All Rights Reserved.
 */
public class Global {
    /**
     * 商品种类数，可增加更多
     */
    public static final int GOODS_NUM = 10;
    /**
     * 每种商品总数，可增加更多
     */
    public static final int GOODS_RESOURCE_NUM = 10000;
    /**
     * 用户数量，可增加更多
     */
    public static final int USERS_NUM = 10;

    /**
     * 用户的默认积分信息
     */
    public static final int USER_DEFAULT_POINTS = 100000;

    /**
     * 默认用户的购买次数
     */
    public static final int DEFAULT_BUY_NUMS = 50000;

    /**
     * 订单编号的前缀
     */
    public static final String ORDER_PREFIX = "order-";

    /**
     * 定义的用户前缀
     */
    public static final String THREAD_PREFIX = "user-";

    /**
     * 用户的默认的购买数量限制
     */
    public static final int BUY_NUMS = 5;
}
