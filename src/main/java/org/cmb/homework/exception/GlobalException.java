package org.cmb.homework.exception;


public class GlobalException extends RuntimeException{

    private int code;

    private String message;

    public GlobalException(String message, int code){
        this.code = code;
        this.message = message;
    }
}
