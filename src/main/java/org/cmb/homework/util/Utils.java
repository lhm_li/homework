package org.cmb.homework.util;

import org.cmb.homework.Global;
import org.cmb.homework.Good;
import org.cmb.homework.User;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Utils {

    private static int good_id = 1;
    private static int user_id = 1;
    private static Map<Integer, String> descriptionMap = new HashMap<>();
    private static final Random RANDOM = new Random();

    static {
        descriptionMap.put(1, "前海开源新经济灵活配置");
        descriptionMap.put(2, "长城行业轮动灵活配置");
        descriptionMap.put(3, "东方阿尔法优势产业混合A");
        descriptionMap.put(4, "东方新能源汽车主题混合");
        descriptionMap.put(5, "东方阿尔法优势产业混合B");
        descriptionMap.put(6, "信诚新兴产业混合");
        descriptionMap.put(7, "海富通成长甄选混合A");
        descriptionMap.put(8, "海富通成长甄选混合C");
        descriptionMap.put(9, "平安鼎泰灵活配置混合");
        descriptionMap.put(10, "创金和信优选回报灵活配置混合");
        descriptionMap.put(11, "易方达蓝筹精选混合");
        descriptionMap.put(12, "华夏军工安全灵活配置");
    }
    /**
     * 生成一个Good对象
     * @return Good对象
     */
    public static Good generateGoodEntity(){
        Good good = new Good();
        good.setDescription(descriptionMap.get(good_id))
                .setId(good_id++)
                .setPrice(RANDOM.nextInt(5) + 1)
                ;
        return good;
    }

    /**
     * 生成一个User对象
     * @return User对象
     */
    public static User generateUserEntity(){
        User user = new User();
        user.setUserID(user_id++);
        user.setRemainingPoints(Global.USER_DEFAULT_POINTS);
        return user;
    }

    /**
     * 获取指定范围内的随机数
     * @param bound 指定范围
     * @return 生成的随机数
     */
    public static int generateRandomInt(int bound){
        return RANDOM.nextInt(bound);
    }

    /**
     * 生成对应的订单编号
     * @return 订单编号
     */
    public static String generateOrderNum(){
        return Global.ORDER_PREFIX + new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date()) + RANDOM.nextInt(10);
    }

}
