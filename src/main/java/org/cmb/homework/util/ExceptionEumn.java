package org.cmb.homework.util;

public enum ExceptionEumn {

    TASK_EXCEPTION(10001, "执行商品购买任务出错"),

    GLBAL_EXCEPTION(20001, "系统未知异常");


    ExceptionEumn(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private final int code;
    private final String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
