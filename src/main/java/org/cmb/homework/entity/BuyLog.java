package org.cmb.homework.entity;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@ToString
public class BuyLog {

    /**
     * 订单号
     */
    private String orderNum;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 购买的商品id
     */
    private Integer goodId;

    /**
     * 商品的购买数量
     */
    private Integer buyNum;

    /**
     * 购买时间
     */
    private Date CreateTime;

}
